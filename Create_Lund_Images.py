import numpy as np
import glob
import os
import read_lund_json as lund
#######################
dir_in = 'data/data_SM/' # to bechanged for different processes
dir_out = 'data/data_images/'
files  = glob.glob(dir_in+'/*')
q = 0 
xval = [0,7]
yval = [-1,8]
Images=[]
for file in files:
    q +=1
    print(f'Processing Image:  {q+1}')
    os.system('./example < %s'%(file))
    if os.path.getsize('jets.json')==0:continue
    reader = lund.Reader('jets.json',100)
    reader.reset()
    img_generator = lund.LundImage(reader,1,25,xval,yval)
    images = img_generator.values()
    Images.append(np.array(images))
    os.remove('jets.json')
    
np.savez_compressed(dir_out+'Images_SM',Images)    
