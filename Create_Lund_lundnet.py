import numpy as np
import glob
import os
import read_lund_json as lund
#######################
dir_in = 'data/data_SM/' # to bechanged for different processes
files  = glob.glob(dir_in+'/*')
q = 0 
open('SM_train_jets.json','w+')
for file in files:
    q +=1
    print(f'Processing Image:  {q+1}')
    os.system('./example_lundnet < %s'%(file))
    if os.path.getsize('jets.json')==0:continue
    reader = lund.Reader('jets.json',100)
    reader.reset()
    os.system('cat jets.json >> SM_train_jets.json')
    os.remove('jets.json')
    if q == 80000: break
os.system('gzip   SM_train_jets.json ')    
